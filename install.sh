#!/bin/bash

# Caddy
sudo apt install -y debian-keyring debian-archive-keyring apt-transport-https
curl -1sLf 'https://dl.cloudsmith.io/public/caddy/stable/gpg.key' | sudo tee /etc/apt/trusted.gpg.d/caddy-stable.asc
curl -1sLf 'https://dl.cloudsmith.io/public/caddy/stable/debian.deb.txt' | sudo tee /etc/apt/sources.list.d/caddy-stable.list
sudo apt update
sudo apt install caddy
sudo systemctl disable --now caddy
sudo systemctl enable --now caddy-api

sudo mkdir -p /etc/caddy
sudo cp Caddyfile /etc/caddy/Caddyfile

# Node.js LTS
curl -fsSL https://deb.nodesource.com/setup_lts.x | sudo -E bash -
sudo apt-get install -y nodejs npm
sudo npm install -g npm

# Python 3
sudo apt install python3 python3-pip
sudo pip install flask

# Setup Mashal
## Share folders
sudo mkdir -p /var/lib/mashal/
sudo mkdir -p /var/lib/mashal/admin/
sudo mkdir -p /var/lib/mashal/client/
sudo mkdir -p /var/lib/mashal/api/
sudo mkdir -p /var/lib/mashal/storage/
sudo cp admin/dist/* /var/lib/mashal/admin/
sudo cp client/* /var/lib/mashal/client/
sudo cp api/* /var/lib/mashal/api/
## User and group
sudo groupadd --system mashal
sudo useradd --system --gid mashal --create-home --home-dir /var/lib/mashal --shell /usr/sbin/nologin --comment "Mashal automation server" mashal
## Executable
sudo cp mashal /usr/bin/mashal
## Service file
sudo cp mashal.service /lib/systemd/system/mashal.service
sudo systemctl daemon-reload
sudo systemctl enable mashal
sudo systemctl start mashal