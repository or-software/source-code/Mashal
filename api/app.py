from flask import Flask

app = Flask(__name__)

# API
@app.route('/api')
def api():
    return {}


if __name__ == "__main__":
    app.run(debug=True)