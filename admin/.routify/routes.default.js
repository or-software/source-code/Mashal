

export default {
  "meta": {},
  "id": "_default",
  "file": {
    "path": "src/routes",
    "dir": "src",
    "base": "routes",
    "ext": "",
    "name": "routes"
  },
  "rootName": "default",
  "routifyDir": import.meta.url,
  "children": [
    {
      "meta": {},
      "id": "_default_index_svelte",
      "name": "index",
      "module": () => import('../src/routes/index.svelte'),
      "file": {
        "path": "src/routes/index.svelte",
        "dir": "src/routes",
        "base": "index.svelte",
        "ext": ".svelte",
        "name": "index"
      },
      "children": []
    },
    {
      "meta": {},
      "id": "_default_test_svelte",
      "name": "test",
      "module": () => import('../src/routes/test.svelte'),
      "file": {
        "path": "src/routes/test.svelte",
        "dir": "src/routes",
        "base": "test.svelte",
        "ext": ".svelte",
        "name": "test"
      },
      "children": []
    }
  ]
}