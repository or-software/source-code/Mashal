# Mashal

## Config Options

### `server`
* default:
```json
{
    "server": {
        "main": {
            "port": 80
        },
        "preview": {
            "port": 8080
        },
        "httpProtocol": 2,
        "tls": {
            "key": "",
            "cert": ""
        }
    }
}
```
**`main`**

*`port`*
* Options: any number, must be an available port
* Default: `80`

**`preview`**

*`port`*
* Options: any number, must be an available port
* Default: `8080`

**`httpProtocol`**
* Options: `2`, `3`
* Default: `2`

Until NodeJS [implements HTTP/3](https://github.com/nodejs/node/issues/38478), using option `3` will fallback to option `2`.

**`tls`**
In order to use HTTPS or HTTP/3, `key` and `cert` must be set.

*`key`*
* Options: path to TLS key ".pem" file
* Default: `""`

*`cert`*
* Options: path to TLS certificate ".pem" file
* Default: `""`
